class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound, :with => :not_found

  private

  # Not found record error handler
  def not_found
    redirect_to(:root, notice: 'Element not found', flash: { color: 'danger' })
  end
end
