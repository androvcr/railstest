class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]

  # GET /books
  def index
    @books = Book.includes(:authors).all.order(:title)
  end

  # GET /books/1
  def show
    @authors = Author.all.order(:last_name) - @book.authors
    @collaboration = Collaboration.new
  end

  # GET /books/new
  def new
    @book = Book.new
    @houses = PublisherHouse.all.order(:name)
  end

  # GET /books/1/edit
  def edit
    @houses = PublisherHouse.all.order(:name)
  end

  # POST /books
  def create
    @book = Book.new(book_params)
    @houses = PublisherHouse.all.order(:name)

    respond_to do |format|
      if @book.save
        format.html { redirect_to(@book, notice: 'Book was successfully created.') }
      else
        format.html { render(:new) }
      end
    end
  end

  # PATCH/PUT /books/1
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to(@book, notice: 'Book was successfully updated.') }
      else
        format.html { render(:edit) }
      end
    end
  end

  # DELETE /books/1
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to(books_url, notice: 'Book was successfully destroyed.', flash: {color: 'warning'}) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.includes(:authors).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:title, :pages, :cover, :published_at, :publisher_house_id)
    end
end
