class CollaborationController < ApplicationController
  
  # POST /collaboration
  def create
    book_id = collaboration_params[:book_id]
    author_id = collaboration_params[:author_id]

    collaborations = Collaboration.where("author_id = ? AND book_id = ?", author_id, book_id)
    book = Book.find(book_id)
    author = Author.find(author_id)

    if (collaborations.any?)  # If collaboration already exist, don't create it again
      redirect_to(book, notice: 'Author is already associated with this book', flash: {color: 'danger'})
    else
      if (book.nil?)
        redirect_error('Book not found')
      else
        if (author.nil?)
          redirect_error('Author not found')
        else    # everything went ok
          book.authors.push(author)
          book.save
          redirect_to(book, notice: 'Author added to this book.')
        end
      end
    end
  end

  # DELETE /collaboration/1
  def destroy
    book_id = collaboration_params[:book_id]
    author_id = collaboration_params[:author_id]
    book =  Book.find(book_id)
    
    collaborations = Collaboration.where("author_id = ? AND book_id = ?", author_id, book_id)
    if (collaborations.any?)  # checks if collaboration already exist
      collaborations.destroy_all
      redirect_to(book, notice: 'Author was removed', flash: {color: 'warning'})
    else
      redirect_to(book, notice: 'Author couldn\'t be removed', flash: {color: 'warning'})
    end
  end

private
  # Never trust parameters from the scary internet, only allow the white list through.
  def collaboration_params
    params.require(:collaboration).permit(:book_id, :author_id)
  end

  # in case of error redirect to root
  def redirect_error (message)
    redirect_to(:root, notice: message, flash: {color: 'danger'})
  end
end
