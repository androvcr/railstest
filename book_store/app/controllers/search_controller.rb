class SearchController < ApplicationController

  # GET /search
  def index
    if (params[:term].nil? || params[:term].length < 3)
      redirect_to(:root, notice: 'Search query needs 3 chars minumum', flash: {color: 'danger'})
    else
      search = params[:term]
      @books = Book.where("title like ?", "%#{search}%").order(:title)
      @authors = Author.where("first_name like ? OR last_name like ?", "%#{search}%", "%#{search}%").order(:last_name)
      @houses = PublisherHouse.where("name like ?", "%#{search}%").order(:name)
    end
  end

  private

  # Never trust parameters from the scary internet, only allow the white list through.
  def search_params
    params.require(:search).permit(:term)
  end

end
