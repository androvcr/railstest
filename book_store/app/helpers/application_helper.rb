module ApplicationHelper

  # Get the term search value or return a default value
  def get_search_params (params)
    params[:term].nil? ? "Search" : params[:term]
  end

  # fancy icon for the publisher home path
  def publisher_home_link
    link_to '<button type="button" class="btn btn-info btn-lg">
        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
    </button>'.html_safe, publisher_houses_path
  end

  # fancy icon for the authors home path
  def authors_home_link
    link_to '<button type="button" class="btn btn-info btn-lg">
        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
    </button>'.html_safe, authors_path
  end

  # fancy icon for the books home path
  def books_home_link
    link_to '<button type="button" class="btn btn-info btn-lg">
        <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
    </button>'.html_safe, books_path
  end

  # fancy edit icon edit models
  def edit_icon_link_to (link_path)
    link_to '<button type="button" class="btn btn-info btn-md">
        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
    </button>'.html_safe, link_path
  end

  # fancy delete icon edit models
  def delete_icon_link_to (element)
    link_to '<button type="button" class="btn btn-danger btn-md">
        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
    </button>'.html_safe, element, method: :delete, data: { confirm: 'Are you sure?' }
  end

  # fancy create_new icon edit models
  def create_icon_link_to (link_path)
    link_to '<button type="button" class="btn btn-success btn-md">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            </button>'.html_safe, link_path
  end

  # fancy save icon edit models
  def save_button
    '<button type="submit" class="btn btn-success btn-md">
        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
    </button>'.html_safe
  end

  # fancy cancel icon edit models
  def cancel_button (link_path)
    link_to '<button type="button" class="btn btn-danger btn-md">
              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
          </button>'.html_safe, link_path
  end

  # Section title for panels, it also applied a fancy icon
  def section_title (title)
    "<h3><span class=\"glyphicon glyphicon-pushpin\" aria-hidden=\"true\"></span> #{title}</h3>".html_safe
  end
  
end