# Author Class
class Author < ApplicationRecord
  # Dependencies
  has_many :collaborations, dependent: :destroy
  has_many :books, through: :collaborations

  # Validations
  validates :first_name, presence: true, length: { minimum: 5, maximum: 35 }
  validates :last_name, presence: true, length: { minimum: 5, maximum: 50 }
  validates :birthdate, presence: true

  # Alias
  alias_attribute :search_name, :author_name

  ## Public Methods

  # Author name returns the last_name, first_name format
  def author_name
    "#{last_name}, #{first_name}"
  end

  # Get 3 random books for the current Author
  def sample_books
    books.sample(3).sort! { |a,b| a.title.downcase <=> b.title.downcase }
  end

  # books ordered by title
  def sort_books 
    books.order(:title)
  end
end
