# Book Class
class Book < ApplicationRecord
  # Dependencies
  belongs_to :publisher_house
  has_many :collaborations, dependent: :destroy
  has_many :authors, through: :collaborations

  # Validations
  validates :title, presence: true, uniqueness: true, length: { minimum: 10, maximum: 150 }
  validates :pages, presence: true, numericality: {only_integer: true, greater_than: 0}
  validates :cover, presence: true, url: true
  validates :published_at, presence: true

  # Alias
  alias_attribute :search_name, :title

  # sort authors by last name
  def sort_authors
    authors.order(:last_name)
  end
  
end
