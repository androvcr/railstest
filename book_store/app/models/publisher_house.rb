# Publisher House Class
class PublisherHouse < ApplicationRecord
  # Dependencies
  has_many :books, dependent: :destroy

  #Validations
  validates :name, presence: true, uniqueness: true, length: { maximum: 150 }

  # Alias
  alias_attribute :search_name, :name

  ## Public Methods

  # order book by title
  def sort_books 
    books.order(:title)
  end
end
