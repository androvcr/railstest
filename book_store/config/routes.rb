Rails.application.routes.draw do
  resources :publisher_houses
  resources :authors
  resources :books
  
  get '/search', to: 'search#index'
  post '/collaboration', to: 'collaboration#create'
  delete '/collaboration', to: 'collaboration#destroy'

  root to: 'publisher_houses#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
