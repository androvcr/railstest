class AddRelationShipFromBooksToPublisherHouse < ActiveRecord::Migration[5.0]
  def up
    add_column :books, :publisher_house_id, :integer
  end

  def down
    remove_column :books, :publisher_house_id
  end
end
