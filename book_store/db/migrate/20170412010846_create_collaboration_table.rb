class CreateCollaborationTable < ActiveRecord::Migration[5.0]
  def up
    create_table :collaborations do |t|
      t.integer :author_id
      t.integer :book_id
    end
  end

  def down
    dropt_table :collaborations
  end
end
