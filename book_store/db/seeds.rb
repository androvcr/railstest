# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


ActiveRecord::Base.establish_connection
ActiveRecord::Base.connection.tables.each do |table|
  next if table == 'schema_migrations'

  # SQLite
  ActiveRecord::Base.connection.execute("DELETE FROM #{table}")
end

# amazon URI for images
image_url = 'https://images-na.ssl-images-amazon.com/images/I/'

# Editorial Samples
houses_list = ['Chayotito Editorial', 'Super Libros', 'Tamarindo Store', 'New Way Thinking', 'NTICPAEV Books', 'DarkBooks', 'Old Fashioned Books', 'Mega Editorial']
houses_object = []
houses_list.each do |name|
  h = PublisherHouse.create( name: name)
  houses_object.push(h)
end

# Authors Samples
authors_list = [  {name: 'Andres', last: 'Rojas', date: '29/10/1983'},
                  {name: 'Stefanny', last: 'Castro', date: '04/11/1984'},
                  {name: 'Tomas', last: 'Bastante', date: '11/12/1890'},
                  {name: 'Sofia', last: 'Morales', date: '1/3/1981'},
                  {name: 'Pamela', last: 'Araya', date: '1/3/1981'},
                  {name: 'Carlos', last: 'Corella', date: '1/3/1981'},
                  {name: 'Josue', last: 'Hidalgo', date: '1/3/1981'},
                  {name: 'Alejandro', last: 'Coronado', date: '1/3/1981'},
                  {name: 'Roberto', last: 'Salazar', date: '1/3/1981'},
                  {name: 'Ricardo', last: 'Ugalde', date: '1/3/1981'},
                  {name: 'Romario', last: 'Ortega', date: '1/3/1981'},
                  {name: 'Cindy', last: 'Morera', date: '1/3/1981'}
                   ]
authors_object = []
authors_list.each do |data|
  a = Author.create( first_name: data[:name], last_name: data[:last], birthdate: data[:date])
  authors_object.push(a)
end

# Books Samples
books_list = [ {name: 'Cocina Zombie', pages: 100, cover: '51PGu841voL.jpg', date: '01/01/2000'},
                {name: 'Cocina Mediterranea', pages: 200, cover: '41nYEMfvoEL.jpg', date: '15/01/2000'},
                {name: 'Tragos Faciles', pages: 200, cover: '512MtS00cFL.jpg', date: '15/01/2000'},
                {name: 'Secretos de Costa Rica', pages: 200, cover: '51hiyZAxDAL.jpg', date: '15/01/2000'},
                {name: 'Soy yo o me le parezco', pages: 200, cover: '51idSm4KvzL.jpg', date: '15/01/2000'},
                {name: 'Bombas Caseras', pages: 200, cover: '61ITyv1lTML.jpg', date: '15/01/2000'},
                {name: 'Martial Arts', pages: 200, cover: '41Af4JnmS8L.jpg', date: '15/01/2000'},
                {name: 'Tang Soo Do, black belt manual', pages: 200, cover: '51ki6dQPWLL.jpg', date: '15/01/2000'},
                {name: 'Como programar en Rails', pages: 200, cover: '51plnK1YpdL.jpg', date: '15/01/2000'},
                {name: 'SQlite', pages: 200, cover: '41RAmCH3EmL.jpg', date: '15/01/2000'},
                {name: 'Posgress SQL', pages: 200, cover: '51C2AkS3QdL.jpg', date: '15/01/2000'},
                {name: 'AngularJS for Dummies', pages: 200, cover: '41LuXiRn1eL.jpg', date: '15/01/2000'},
                {name: 'Al chile?!', pages: 200, cover: '51mK6BAFAWL.jpg', date: '15/01/2000'},
                {name: 'Ciudad Quesada desde adentro', pages: 200, cover: '5151jMdx7TL.jpg', date: '15/01/2000'},
                {name: 'Kamasutra para conejos', pages: 200, cover: '518eq5NjZkL.jpg', date: '15/01/2000'},
                {name: 'Si supieras volar', pages: 200, cover: '51jG8zOATIL.jpg', date: '15/01/2000'},
                {name: 'Trump o Trompadas', pages: 200, cover: '51rV-3xwEJL.jpg', date: '15/01/2000'},
                {name: 'Donde esta mi cerveza', pages: 200, cover: '51ZOFF%2Bj4BL.jpg', date: '15/01/2000'},
                {name: 'Kurzo de hortografia', pages: 200, cover: '51KTckqNeKL.jpg', date: '15/01/2000'},
                {name: 'El Quijote moderno', pages: 200, cover: '41EII0L2ciL.jpg', date: '15/01/2000'}
              ]

books_list.each do |data|
  b = Book.create( title: data[:name], pages: data[:pages], cover: (image_url + data[:cover]), published_at: data[:date])
  b.publisher_house = houses_object.sample
  b.authors = authors_object.sample(rand(1..5))   # pick from 1 to 5 authors and assign it to the book
  b.save
end