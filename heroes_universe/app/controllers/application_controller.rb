class ApplicationController < ActionController::API
  # Simple authentication
  include ActionController::HttpAuthentication::Basic::ControllerMethods
  http_basic_authenticate_with name: "my_user", password: "my_password"

  # Error Handling
  
  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  rescue_from Exception, with: :render_error_response

  # Public methods

  # Helper for the controllers to paginate the information and sets the correct returned values
  def set_headers_pagination (data_set, object_class, params)
    response.set_header('X-Total', object_class.count)
    response.set_header('X-Total-Pages', data_set.total_pages)
    response.set_header('X-Page', data_set.current_page)
    response.set_header('X-Per-Page', params[:per_page])
    response.set_header('X-Next-Page', data_set.next_page)
    response.set_header('X-Prev-Page', data_set.prev_page)
  end

  # set html response in 200
  def get_action_response
    response.set_header('status-code', 200)
  end

  # set html response in 201
  def post_action_response
    response.set_header('status-code', 201)
  end

  # set html response in 200
  def put_action_response
    response.set_header('status-code', 200)
  end

  # set html response in 204
  def delete_action_response
    response.set_header('status-code', 204)
  end

  private

  # Active record not found error handler
  def not_found (message)
    get_action_response
    render json: {error: "#{message}"}
  end

  # Orders errors handler
  def render_error_response (message)
    get_action_response
    message ||= "Bad Request"
    render json: {error: message}
  end

end
