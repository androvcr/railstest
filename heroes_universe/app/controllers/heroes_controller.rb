class HeroesController < ApplicationController
  before_action :set_hero, only: [:show, :update, :destroy]

  # GET /heroes
  def index
    ## default values in case that aren't provided
    page = params[:page].nil? ? 1 : params[:page].to_i
    per_page = params[:per_page].nil? ? 10 : params[:per_page].to_i

    heroes = Heroe.page(page).per(per_page)
    set_headers_pagination(heroes, Heroe, params)
    get_action_response
    render json: heroes
  end

  # GET /heroes/1
  def show
    get_action_response
    render json: @hero
  end

  # POST /heroes
  def create
    @hero = Heroe.new()
    real_universe = Universe.find(hero_params[:universe])
    @hero.name = hero_params[:name]
    @hero.real_name = hero_params[:real_name]
    @hero.universe = real_universe
    @hero.species = hero_params[:species]
    @hero.abilities = hero_params[:abilities]

    if @hero.save
      post_action_response
      render json: @hero
    else
      render_error_response
    end
  end

  # PATCH/PUT /heroes/1
  def update
    if @hero.update(hero_params)
      put_action_response
      render json: @hero
    else
      render_error_response
    end
  end

  # DELETE /heroes/1
  def destroy
    delete_action_response
    @hero.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hero
      @hero = Heroe.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def hero_params
      params.require(:hero).permit(:id, :name, :real_name, :universe, :species, abilities: [] )
    end

end
