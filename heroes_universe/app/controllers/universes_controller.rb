class UniversesController < ApplicationController
  before_action :set_universe, only: [:show, :update, :destroy]
  
  # GET /universes
  def index
    # default values in case that aren't provided
    page = params[:page].nil? ? 1 : params[:page].to_i
    per_page = params[:per_page].nil? ? 10 : params[:per_page].to_i
    
    universes = Universe.page(page).per(per_page)
    set_headers_pagination(universes, Universe, params)
    get_action_response
    render json: universes
  end

  # GET /universes/1
  def show
    get_action_response
    render json: @universe
  end

  # POST /universes
  def create
    @universe = Universe.new(universe_params)

    if @universe.save
      post_action_response
      render json: @universe
    else
      render_error_response
    end
  end

  # PATCH/PUT /universes/1
  def update
    if @universe.update(universe_params)
      put_action_response
      render json: @universe
    else
      render_error_response
    end
  end

  # DELETE /universes/1
  def destroy
    delete_action_response
    @universe.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_universe
      @universe = Universe.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def universe_params
      params.require(:universe).permit(:name)
    end

end
