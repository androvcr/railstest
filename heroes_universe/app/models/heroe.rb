class Heroe < ApplicationRecord
  before_save :default_values
  
  # Dependencies
  belongs_to :universe

  # Validations
  validates :name, presence: true, length: { minimum: 3, maximum: 30 }, uniqueness: true
  validates :real_name, presence: true, length: { minimum: 3, maximum: 30 }
  validates :universe, presence: true

  # Serializer
  serialize :abilities, Array

  def default_values
    self.abilities ||= []
    self.species ||= "Human"
  end
end
