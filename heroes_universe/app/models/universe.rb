class Universe < ApplicationRecord
  # Dependencies
  has_many :heroes, dependent: :destroy, class_name: "Heroe"

  # Validations
  validates :name, presence: true, length: { minimum: 5, maximum: 40 }, uniqueness: true

  # Serializers
  serialize :name
end