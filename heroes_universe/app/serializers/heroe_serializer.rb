class HeroeSerializer < ActiveModel::Serializer
  belongs_to :universe

  attributes :id, :name, :real_name, :universe, :species, :abilities
end
