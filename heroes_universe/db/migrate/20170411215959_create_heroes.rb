class CreateHeroes < ActiveRecord::Migration[5.0]
  def up
    create_table :heroes do |t|
      t.string :name
      t.string :real_name
      t.integer :universe_id
      t.string :species
      t.text :abilities
      t.timestamps
    end
  end

  def down
    drop_table :heroes
  end
end
