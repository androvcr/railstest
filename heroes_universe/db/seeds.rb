# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


ActiveRecord::Base.establish_connection
ActiveRecord::Base.connection.tables.each do |table|
  next if table == 'schema_migrations'

  # SQLite
  ActiveRecord::Base.connection.execute("DELETE FROM #{table}")
end

universes = ['DC Comics', 'Marvel', 'Dragon Ball', 'One Piece', 'Naruto', 'Bleach']
universes_object = []
universes.each do |name|
  h = Universe.create( name: name)
  universes_object.push(h)
end

heroes = [
  {name: 'Goku', real_name: 'Kakaroto', species: 'Sayayin', universe: 2, abilities: ['strong', 'martial arts', 'keep working']},
  {name: 'IronMan', real_name: 'Tonny Stark', species: 'Human', universe: 1, abilities: ['money', 'super weapons']}
]
heroes.each do |data|
  Heroe.create(name: data[:name], real_name: data[:real_name], species: data[:species], universe: universes_object[data[:universe]], abilities: data[:abilities])
end
