# README #

Andres Rojas, RubyOnRails Test Projects

## What is this repository for? ##

* Heroes API
* Book Store App

## How do I get set up? ##

* I used Vagrant with an already build machine with Rails5 (https://atlas.hashicorp.com/techbytes/boxes/rails5ubuntu)

## Heroes API Project ##
We have a lot of Heroes Universes in this world, Marvel, DC Comics, One Piece, Dragon Ball, Tomb Raider, Mortal Kombat, etc, etc, etc, and for us is complicated manage all the characters that belongs to each universe. That's why we created this RestFul JSON API, to manage it in a easy way and provide a single data center for all the customs applications that may need our effort.

* Project Delivery Date: 2017-04-14

### Available Restful Routes ###
* GET     /universes       => gets all the available universes
* GET     /universes/:id   => gets the universe with the identification # :id
* POST    /universes       => creates a new universe
* PUT     /universes/:id   => edits the universe with the identification # :id
* DELETE  /universes/:id   => deletes the universe with the identification # :id

* GET     /heroes       => gets all the available heroes
* GET     /heroes/:id   => gets the hero with the identification # :id
* POST    /heroes       => creates a new hero
* PUT     /heroes/:id   => edits the hero with the identification # :id
* DELETE  /heroes/:id   => deletes the hero with the identification # :id

More information about the params and responses, please check our video. 

BTW: I am using Postman as a API tester. :)

Universes Routes -> https://screencast.com/t/LoeJbC0q

Heroes Routes -> https://screencast.com/t/HRbnKfD69qR

Not implemented: Unit Testing (seems that something was not working on my enviroment or maybe I am the problem, or both)


## My dear library Project ##
Which is the correct book?, when it was writter?, who helped to write them?, and have those guys another books? sometimes get all the information is messy, it's not available or it's hard find. My Dear Library saves the day. Search, follow, add, remove, modify... this application helps you to manage all your book store needs.
With this application you can organize your books, authors and publisher houses in order to have them updated and connected.

Take a look our super cool video -> https://www.screencast.com/t/h5pO0Z6q47

* Project Delivery Date: 2017-04-14


## HOW TO INSTALL THOSE ##
1. Download vagrant and virtualbox
2. Install the rails machine (https://atlas.hashicorp.com/techbytes/boxes/rails5ubuntu)
3. Download this repository 
4. In the root folder, on the command line: $ vagrant up && vagrant ssh
5. when you are inside of the vagrant machine: $ cd /vagrant/

### A. for Universes API ###

* $ cd /heroes_universes
* $ bundle install
* $ rake db:migrate
* $ rake db:seed
* $ sh startServer.sh
It's going to run in your host machine at port 8080 ( http://localhost:8080/ )
* Open Postman and enjoy

### B. for My Dear Library ###

* $ cd /book_store
* $ bundle install
* $ rake db:migrate
* $ rake db:seed
* $ sh startServer.sh
It's going to run in your host machine at port 8080 ( http://localhost:8080/ )
* Open your browser and have fun


Thanks :)